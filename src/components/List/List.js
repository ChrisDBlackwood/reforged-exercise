import React, { Component } from 'react'
import './styles.css'

export default class List extends Component {
    render() {
        return (
            <li key={this.props.firstName}>
                {this.props.firstName} {this.props.lastName}
            </li>
        )
    }
}
