import React, { Component } from 'react'
import List from '../../components/List/List'
import './styles.css'

export default class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        let data = require('../../assets/characters.json');
        this.setState({
            data: data
        });
    }


    render() {
        let sortedList = this.state.data.sort(function (a, b) {
            if (a.firstName < b.firstName) { return -1; }
            if (a.firstName > b.firstName) { return 1; }
            return 0;
        })


        return (
            <ul>
            {sortedList.map(item => (
              <List firstName={item.firstName} lastName={item.lastName}/>
            ))}
            </ul>
        )
    }
}
